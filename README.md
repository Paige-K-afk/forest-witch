# Forest Witch

# Installation
Download this repo and unzip the zip file.
The game should be ready-to-play as soon as the zip is downloaded.

# Usage
You can play the game, that's really all that's even possible, Good luck.

# The version number of unreal used to produce the game. 
5.0.3

# Description
Forest witch is a stealth horror inspired game, combining the gameplay of FNAF: Security Breach with the aesthetics of Inscrypion, that was developed as my coursework for the Game Behaviour module at the University of derby.

# How to play
Brief instructions for how to run the game. 
Go into the file labeled 'Standalone'.
Go into the file labeled 'Windows'.
Double click 'StealthWitch.exe'.
Press 'ok' on the Nvidia driver error; it's nothing.
Tank the loading time. And you should be in the game.
It works on my machine.


# A brief description of the game. 
You are a forest witch.
All of your animal friends have started acting weird, but if you can make it to your grotto, you can cast a spell to fix this.
This is a simple stealth game
The animals (Enemies) have patrols; you need to avoid them.
Wolves will kill you (send you back to the start), birds will allert wolves to your presence.
Water is a hazard (but won't consume a life, so follow that log into the river and see what that submerged bridge does). If you go above the waist in water, you'll be swept back to the start.
You have 4 lives. Dropping to zero lives will set you to game over (going into water does not consume a life).


# A list of all controls for the game. 
WASD - Move
Space - Jump
Esc or L - reset player position to spawn (only use when stuck, unless you want to go back to the start)
Run into things to move them. Just shove objects that you think can be shoved.


# Roadmap
The game is complete, but I may re-make it at some point.


# Support
https://gitlab.com/Paige-K-afk
https://github.com/Paige-K-afk
If you can send messages on github or gitlabs be my guest.



# Contributing
No attempting to contribute please. It's a complete project. You will be rejected.

# Authors and acknowledgment
Author: Me (Paige)
Acknowledements:
I'd like to thank my lecturer for the game behavior module at the University of Derby in 2022-2023.

